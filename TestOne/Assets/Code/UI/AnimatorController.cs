﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Animator))]
public class AnimatorController : MonoBehaviour
{
    Animator animator;
    Action onAnimFinished;

    void Start ()
    {
        Init();
    }

    public void Init()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        animator.enabled = false;
    }

    public void Play(string animName, Action onPlayed = null)
    {
        onAnimFinished = onPlayed;
        if (!HasAnimation(animName))
        {
            Log.Warning("No Such Anim: " + animName);
            return;
        }
        Log.Info("Playing Anim: " + animName);
        animator.enabled = true;
        animator.Play(animName);
        Invoke("AnimFinished", animator.GetCurrentAnimatorStateInfo(0).length);
    }

    public bool HasAnimation(string animName)
    {
        foreach (var animClip in animator.runtimeAnimatorController.animationClips)
            if (animClip.name == animName)
                return true;
        return false;
    }

    void AnimFinished()
    {
        Log.Info("Anim Finished");
        animator.enabled = false;
        onAnimFinished.Fire();
    }
}
