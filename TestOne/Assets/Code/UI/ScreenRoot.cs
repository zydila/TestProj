﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenRoot : MonoBehaviour
{
    [SerializeField] public RectTransform screenTransform;

    void Start()
    {
        ManagerHolder.GetInstance<ScreenManager>().SetScreenRoot(this);
    }

    public T SpawnScreen<T>(RectTransform screenPrefab) where T : ScreenBase
    {
        return Instantiate(screenPrefab, screenTransform).GetComponent<T>();
    }
}
