﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputLockController : MonoBehaviour
{
    [SerializeField] CanvasGroup screenCanvasGroup;

    InputManager inputManager;
    bool isCurrentlyLocked;
    
    void Start()
    {
        inputManager = ManagerHolder.GetInstance<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        bool localLock = inputManager.IsInputLock();
        if (localLock != isCurrentlyLocked)
            if (localLock)
                Lock();
            else
                Unlock();
    }

    void Lock()
    {
        isCurrentlyLocked = true;
        screenCanvasGroup.blocksRaycasts = false;
    }

    void Unlock()
    {
        isCurrentlyLocked = false;
        screenCanvasGroup.blocksRaycasts = true;
    }
}
