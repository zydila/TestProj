﻿using UnityEngine.UI;

public static class UIExtensions
{
    public static void SetLocalizedText(this Text uiText, string localizedText)
    {
        uiText.text = localizedText;
    }

    public static void SetTextByKey(this Text uiText, string textKey)
    {
        uiText.SetLocalizedText(textKey.Localize());
    } 

    public static void SetTextByKeyFormat(this Text uiText, string textKey, params object[] args)
    {
        uiText.SetLocalizedText(textKey.LocalizeFormat(args));
    }
}