﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class BaseExtentions
{
    public static string Localize(this string textKey)
    {
        return Language.GetLocalizationByKey(textKey);
    }

    public static string LocalizeFormat(this string textKey, params object[] args)
    {
        return string.Format(textKey.Localize(), args);
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    public static void Fire(this Action action)
    {
        if (action == null)
            return;
        Delegate[] delegates = action.GetInvocationList();
        for (int index = 0; index < delegates.Length; ++index)
        { Action delegateAction = (Action)delegates[index];
            if (IsSafeDelegate(delegateAction))
                delegateAction();
        }
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    public static void Fire<T>(this Action<T> action, T param)
    {
        if (action == null)
            return;
        Delegate[] delegates = action.GetInvocationList();
        for (int index = 0; index < delegates.Length; ++index)
        {
            Action<T> delegateAction = (Action<T>)delegates[index];
            if (IsSafeDelegate(delegateAction))
                delegateAction(param);
        }
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    public static void Fire<T1, T2>(this Action<T1, T2> action, T1 param1, T2 param2)
    {
        if (action == null)
            return;
        Delegate[] delegates = action.GetInvocationList();
        for (int index = 0; index < delegates.Length; ++index)
        {
            Action<T1, T2> delegateAction = (Action<T1, T2>)delegates[index];
            if (IsSafeDelegate(delegateAction))
                delegateAction(param1, param2);
        }
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    static bool IsSafeDelegate(Delegate del)
    {
        object target = del.Target;
        if (target == null)
        {
            if (!del.Method.IsStatic)
            {
                Log.Warning(string.Format("Delegate target is null along with not static method: {0} {1}", del.Method.Name, del.Method.DeclaringType));
                return false;
            }
        }
        else if ((target is UnityEngine.Object) && target.Equals(null))
        {
            Log.Warning(string.Format("Delegate target was destroyed in native side but exist managed object: {0} {1}\n{2}", del.Method.Name, del.Method.DeclaringType, StackTraceUtility.ExtractStackTrace()));
            return false;
        }
        return true;
    }
}
