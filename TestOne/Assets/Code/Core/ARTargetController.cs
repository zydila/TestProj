﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARTargetController : MonoBehaviour
{
    public RectTransform RootTransform { get; private set; }

    void Start()
    {
        RootTransform = GetComponent<RectTransform>();
    }

    public T SpawnPrefab<T>(RectTransform prefab, Vector3 offset = new Vector3(), Quaternion rotation = new Quaternion()) where T : Object
    {
        return SpawnPrefab(prefab, offset, rotation).GetComponent<T>();
    }

    public RectTransform SpawnPrefab(RectTransform prefab, Vector3 offset = new Vector3(), Quaternion rotation = new Quaternion())
    {
        return Instantiate(prefab, offset, rotation, RootTransform);
    }

    public void RemoveAllChildren()
    {
        if (RootTransform.childCount == 0)
            return;
        for (int i = RootTransform.childCount - 1; i >= 0; i--)
            Destroy(RootTransform.GetChild(i));
    }
}
