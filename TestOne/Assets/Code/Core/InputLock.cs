﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputLock
{
    InputManager inputManager;

    public InputLock()
    {
        inputManager = ManagerHolder.GetInstance<InputManager>();
    }

    public void Unlock()
    {
        inputManager.UnlockInput(this);
    }
}

