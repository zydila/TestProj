﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventDispatcher : ManagerBase
{
    EventDispatcherBehavior behavior;

    public EventDispatcher()
    {
        if (!Application.isPlaying)
            return;
        behavior = UnityEngine.Object.FindObjectOfType<EventDispatcherBehavior>();
        behavior.Dispatcher = this;
    }

    public void StartCoroutine(IEnumerator coroutine)
    {
        behavior.StartCoroutine(coroutine);
    }

    public class InternalDispatcher : MonoBehaviour
    {
        EventDispatcher dispatcher;
        public EventDispatcher Dispatcher
        {
            get
            {
                if (dispatcher == null)
                    dispatcher = ManagerHolder.GetInstance<EventDispatcher>();
                return dispatcher;
            }
            set
            {
                dispatcher = value;
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

    }
}
