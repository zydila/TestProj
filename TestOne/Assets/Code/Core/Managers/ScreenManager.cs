﻿using System.Collections;
using UnityEngine;
using System;

public class ScreenManager : ManagerBase
{
    public ScreenBase currentScreen;
    ScreenBase previousScreen;
    ScreenRoot screenRoot;

    public void SetScreenRoot(ScreenRoot root)
    {
        screenRoot = root;
    }

    public ScreenManager()
    {
        Log.Info("ScreneManager.CONSTRUCT");
        FindScreenRoot();
    }

    public T OpenScreen<T>(Action onOpened = null) where T : ScreenBase
    {
        Type type = typeof(T);
        string screenPath = "UI/Prefabs/Screens/" + type.ToString();
        if (currentScreen != null &&  (currentScreen is T))
        {
            Log.Warning("Sreen already Opened: " + screenPath);
            currentScreen.Init();
            return (T)currentScreen;
        }
        Log.Info("Open Screne: " + screenPath);
        RectTransform prefab = Resources.Load<RectTransform>(screenPath);
        if(prefab == null)
        {
            Log.Error("SCREEN NOT FOUND");
            return null;
        }
        T screen = screenRoot.SpawnScreen<T>(prefab);
        screen.Init();
        if(screen == null)
            Log.Warning("SCREEN DOESN'T HAVE SCRIPT");
        var inputLock = ManagerHolder.GetInstance<InputManager>().LockInput();
        HidePreviousScreen(() =>
        {
            currentScreen = screen;
            currentScreen.ShowScreen(() => 
            {
                inputLock.Unlock();
                onOpened.Fire();
            });
        });
        return screen;
    }

    void HidePreviousScreen(Action onClosed)
    {
        previousScreen = currentScreen;
        if (previousScreen != null)
            previousScreen.HideScreen(() =>
            {
                previousScreen.gameObject.SetActive(false);
                onClosed.Fire();
            });
        else
            ManagerHolder.GetInstance<EventDispatcher>().StartCoroutine(DelayedAction(onClosed));
    }

    IEnumerator DelayedAction(Action action)
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        action.Fire();
    }

    void FindScreenRoot()
    {
        GameObject screenGO = GameObject.Find("Canvas/Screen");
        if (screenGO == null)
        {
            Log.Warning("No Screen root found");
        }
        screenRoot = screenGO.GetComponent<ScreenRoot>();
    }
}
