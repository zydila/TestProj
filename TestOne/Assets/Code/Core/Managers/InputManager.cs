﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : ManagerBase
{
    HashSet<InputLock> inputLocks = new HashSet<InputLock>();

    public InputLock LockInput()
    {
        InputLock inputLock = new InputLock();
        inputLocks.Add(inputLock);
        return inputLock;
    }

    public void UnlockInput(InputLock inputLock)
    {
        inputLocks.Remove(inputLock);
    }

    public bool IsInputLock()
    {
        return inputLocks.Count > 0;
    }
}
