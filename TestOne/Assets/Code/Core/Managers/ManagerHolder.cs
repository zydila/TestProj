﻿using System;
using System.Collections.Generic;

public static class ManagerHolder
{
    static Dictionary<string, ManagerBase> managers = new Dictionary<string, ManagerBase>();

    public static T GetInstance<T>() where T : ManagerBase, new()
    {
        string managerType = typeof(T).ToString();
        ManagerBase manager = new ManagerBase();
        if (managers.ContainsKey(managerType))
            managers.TryGetValue(managerType, out manager);
        else
        {
            manager = new T();
            AddManager<T>(manager);
        }
        return (T)manager;
    }

    static void AddManager<T>(ManagerBase manager) where T : ManagerBase
    {
        Type managerType = typeof(T);
        managers.Add(managerType.ToString(), manager);
    }
}